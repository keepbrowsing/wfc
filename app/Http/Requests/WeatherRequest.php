<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WeatherRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            'city' => 'required|string',
        ];
    }

    /**
     * @return array
     */
    public function messages() : array
    {
        return [
            'city.*' => 'Please provide a valid city name.',
        ];
    }

    /**
     * @return array
     */
    public function data() : array
    {
        return [
            'city' => $this->input('city'),
        ];
    }
}
