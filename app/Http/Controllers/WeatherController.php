<?php

namespace App\Http\Controllers;

use App\Http\Requests\WeatherRequest;
use App\Plugins\Weather\Exceptions\WeatherException;
use App\Plugins\Weather\WeatherManager;
use Illuminate\Http\JsonResponse;

class WeatherController extends Controller
{
    /**
     * @var WeatherManager
     */
    private WeatherManager $weatherManger;

    /**
     * WeatherController constructor.
     * @param WeatherManager $weatherManager
     */
    public function __construct(WeatherManager $weatherManager)
    {
        $this->weatherManger = $weatherManager;
    }

    /**
     * @param WeatherRequest $request
     * @return mixed
     */
    public function getWeatherForCity(WeatherRequest $request)
    {
        try {
            return $this->weatherManger->getByCityName($request->data()['city']);
        } catch (WeatherException $exception) {
            abort(JsonResponse::HTTP_NOT_FOUND, 'City not found.');
        }
    }
}
