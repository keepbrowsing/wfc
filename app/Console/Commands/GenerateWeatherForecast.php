<?php

namespace App\Console\Commands;

use App\Plugins\Weather\Exceptions\WeatherException;
use App\Plugins\Weather\WeatherManager;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class GenerateWeatherForecast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:forecast {cities}';

    private WeatherManager $weatherManager;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates weather forecast for next five days for the provided cities';

    /**
     * Create a new command instance.
     *
     * @param WeatherManager $weatherManager
     */
    public function __construct(WeatherManager $weatherManager)
    {
        parent::__construct();
        $this->weatherManager = $weatherManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Convert comma seperated cities into array.
        $cities = explode(',', $this->argument('cities'));
        // Make sure there are no spaces in the provided city names.
        $cities = array_map('trim', $cities);

        try {
            $response = collect($cities)->mapWithKeys(function ($city) {
                return
                    [
                        $city =>
                            urldecode(
                                http_build_query(
                                    array_column(
                                        $this->weatherManager->getByCityName($city),
                                        'weather_state_name',
                                        'applicable_date',
                                    ),
                                    '',
                                    PHP_EOL
                                )
                            )
                    ];
            });
        } catch (WeatherException $exception) {
            $this->error($exception->getMessage());
            exit;
        }

        // Present the response into a table.
        [$headers, $rows] = $this->generateTableFromResponse($response);
        $this->info("Weather Forecast For Next 5 Days:");
        $this->table($headers, $rows);
    }

    /**
     * Builds table with the weather api response.
     *
     * @param Collection $response
     * @return array
     */
    public function generateTableFromResponse(Collection $response) : array
    {
        $headers = ['City', 'Weather'];
        $rows = $response->map(function ($city, $weatherStatus) {
            return ['Weather' => str_replace('+', ' ', $weatherStatus), 'City' => $city];
        })->toArray();

        return [$headers, $rows];
    }
}
