<?php

namespace App\Plugins\Weather\Exceptions;

use Exception;

class WeatherException extends Exception
{
    // does not have render() and report() methods yet, since we are not logging error neither
    // converting a given exception into an HTTP response yet
}
