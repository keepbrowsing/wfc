<?php

namespace App\Plugins\Weather;

use App\Plugins\Weather\Exceptions\WeatherException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use Psr\Http\Message\ResponseInterface;

class WeatherManager
{
    protected string $url = 'https://www.metaweather.com/api/location/';
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->url,
            'timeout'  => 10.0,
        ]);
    }

    /**
     * @param string $query
     * @return mixed
     * @throws WeatherException
     */
    public function getWoeid(string $query)
    {
        try {
            $response = $this->client->request('GET', '' . 'search/?query='.$query);
            if ($this->isAValidResponse($response)) {
                $result = json_decode($response->getBody()->getContents());
                if (count($result) > 0) {
                    return $result[0]->woeid;
                }
                throw new WeatherException('Invalid city name.');
            }
        } catch (ClientException $e) {
            throw new WeatherException($e->getMessage());
        }
    }

    /**
     * @param string $url
     * @return ResponseInterface
     */
    public function fetchFromApi(string $url)
    {
        return $this->client->request('GET', '' . $url);
    }

    /**
     * @param string $query
     * @return mixed
     * @throws WeatherException
     */
    private function getWeather(string $query)
    {
        // Find Woeid of city.
        $woeid = $this->getWoeid($query);

        try {
            $response = $this->fetchFromApi($woeid);
            $responseBody = json_decode($response->getBody()->getContents());
            // returns 6 days forecast by default including today. so lets remove first from array.
            // @todo: this function could be made more flexible by passing start date and number of days .
            if (count($responseBody->consolidated_weather) == 6) {
                $weatherFor5Days = $responseBody->consolidated_weather;
                array_shift($weatherFor5Days);
                return $weatherFor5Days;
            } else {
                return $responseBody->consolidated_weather;
            }
        } catch (ClientException $e) {
            throw new WeatherException($e->getMessage());
        }
    }

    /**
     * @param GuzzleResponse $response
     * @return bool
     */
    private function isAValidResponse(GuzzleResponse $response) : bool
    {
        return $response->getStatusCode() === JsonResponse::HTTP_OK;
    }

    /**
     * @param string $city
     * @return mixed
     * @throws WeatherException
     */
    public function getByCityName(string $city)
    {
        /**
         * Returns 5 days weather by default from today.
         */
        return $this->getWeather($city);
    }
}
