import React from 'react';
import ReactDOM from 'react-dom';
import WeatherForm from './WeatherForm';

function Weather() {
    return (
        <div class="bg-white p-4">
        <WeatherForm />
        </div>
    );
}

export default Weather;

if (document.getElementById('weather-app')) {
    ReactDOM.render(<Weather />, document.getElementById('weather-app'));
}
