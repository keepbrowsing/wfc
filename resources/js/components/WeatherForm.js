import React, { Component } from "react";
import axios from 'axios';

class WeatherForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cities : ["brisbane","melbourne","london","sydney","invalid city",''],
            results: [],
            city: 'brisbane',
            loading: false,
        }
        this.fetchWeatherForCity = this.fetchWeatherForCity.bind(this);
    }

    fetchWeatherForCity(e) {
        e.preventDefault();

        this.setState({ loading : true, results: [] });
        axios
            .post(`/api/get-weather`, {city: this.state.city})
            .then(res => {
                const data = res.data
                console.log(data)
                this.setState({ loading : false , results : data });

            })
            .catch((error) => {
                alert(error.response.data.message);
                this.setState({ loading : false });
            })
    }

    render() {
        return (
            <div class="forecast-view">
            <form class="mb-2 content-center">
            <select
            class="bg-blue-500 py-2 mr-2 px-4 rounded"
            name="city"
            value={this.state.city}
              onChange={(e) => this.setState({city: e.target.value})}>
            {this.state.cities.map((city) => <option key={city} value={city}>{city}</option>)}
            </select>
            <button
            onClick={this.fetchWeatherForCity}
            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Get Weather</button>
            {this.state.loading ? <span class="loading">loading..please wait!</span> : ""}

            </form>


            <table class="table-auto">
                <thead>
                    <tr>
                    <th class="px-4 py-2">Date</th>
                    <th class="px-4 py-2">Weather</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.results.map((weather, idx) => {
                        return (
                            <tr>
                            <td class="border px-4 py-2" key={idx}>
                            {weather.applicable_date}
                            </td>
                            <td class="border px-4 py-2" key={idx}>
                            {weather.weather_state_name}
                            </td>
                            </tr>
                    )
                    })}

                </tbody>
                </table>

                </div>
        )
    };
}

export default WeatherForm;
