<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Weather Forecast</title>

        <!-- Fonts -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body class="bg-gray-300">

    <div class="container mx-auto px-4 py-4 pt-200">
        <div id="weather-app" class="w-full max-w-xs">
    </div>

    <script src="{{mix('js/app.js')}}" ></script>
    </body>
</html>
